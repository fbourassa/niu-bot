﻿using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Robot_NIU
{
    public partial class MainWindow : Window
    {
        public SolidColorBrush greenLogBrush;
        public WorkerVerification worker;
        public ObservableCollection<Intervenant> listeIntervenants;

        #region Inits

        public MainWindow()
        {
            InitializeComponent(); 

            initVariables();
            initInterface();
            initWorker();
        }

        public void initVariables()
        {
            greenLogBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#00ff00"));
        }

        public void initInterface()
        {
            //Ajouter les choix d'ordre au combobox
            List<ComboBoxChoix> cbp = new List<ComboBoxChoix>();
            cbp.Add(new ComboBoxChoix("NPME - Médecin", Identifiant.Ordre.NPME));
            cbp.Add(new ComboBoxChoix("NPPH - Pharmacien", Identifiant.Ordre.NPPH));
            cbp.Add(new ComboBoxChoix("NPIN - Infirmier", Identifiant.Ordre.NPIN));
            OrdreProfComboBox.DisplayMemberPath = "_Key";
            OrdreProfComboBox.SelectedValuePath = "_Value";
            OrdreProfComboBox.ItemsSource = cbp;

            listeIntervenants = new ObservableCollection<Intervenant>();
            ListeIntervenantsDatagrid.ItemsSource = listeIntervenants;

            addLog("[Warning] : This program comes with no garantee, any information it " +
                    "may provide should be verified by a human being.", greenLogBrush);

        }

        public void initWorker()
        {
            worker = new WorkerVerification();
            worker.MessageDispatched += worker_MessageDispatched;
            Thread workerThread = new Thread(worker.StartDriver);
            worker.ExecutionCompleted += worker_ExecutionCompleted;
            workerThread.Start();
            addLog("Started IE, do not close prompt/browser", Brushes.Yellow);
        }

        #endregion
                
        #region Écouteurs UI

        private void ComparerIndividuelButton_Click(object sender, RoutedEventArgs e)
        {
            if (validerFormulaireIndividuel())
            {
                char sexe = (SexeComboBox.SelectedIndex == 0) ? 'F' : 'M';
                List<Intervenant> intervenants = new List<Intervenant>();
                List<Identifiant> identifiants = new List<Identifiant>();
                identifiants.Add(new Identifiant(((ComboBoxChoix)OrdreProfComboBox.SelectedItem)._Value.ToString(), IdentifiantTextBox.Text.Trim()));

                Intervenant intervComp = new Intervenant { 
                    Nom = NomTextBox.Text.Trim(),
                    Prenom = PrenomTextBox.Text.Trim(),
                    Naissance = NaissanceTextBox.Text.Trim().Replace("-", "/"), 
                    Sexe = sexe , 
                    Identifiants = identifiants,
                    Statut = "ACTIVE"
                };

                intervComp.PropertyChanged += IntervenantIndividuel_PropertyChange;
                intervenants.Add(intervComp);

                Thread workerThread = new Thread(() => worker.verifierIntervenants(intervenants));
                setCompareBoutonsEnabled(false);
                workerThread.Start();
                addLog("Started verification...", greenLogBrush);
            }
            else
            {
                addLog("Formulaire individuel invalide...", Brushes.Red);
            }
        }
        
        private void ResetFormIdividuelButton_Click(object sender, RoutedEventArgs e)
        {
            NomTextBox.Clear();
            PrenomTextBox.Clear();
            NaissanceTextBox.Clear();
            IdentifiantTextBox.Clear();
            ResultatTextBox.Clear();
            OrdreProfComboBox.SelectedIndex = -1;
            SexeComboBox.SelectedIndex = 0;
        }

        private void ParcourirInputCsvButton_Click(object sender, RoutedEventArgs e)
        {
            InputCsvTextBox.Text = choisirEmplacementFichier();
        }

        private void ImporterCsvButton_Click(object sender, RoutedEventArgs e)
        {
            importerFeuilleCSV();
        }

        private void ViderListeButton_Click(object sender, RoutedEventArgs e)
        {
            listeIntervenants.Clear();
        }

        private void ComparerListeButton_Click(object sender, RoutedEventArgs e)
        {
            Thread workerThread = new Thread(() => worker.verifierIntervenants(listeIntervenants.ToList()));
            setCompareBoutonsEnabled(false);
            workerThread.Start();
            addLog("Started verification...", greenLogBrush);
        }

        private void PressePapierButton_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder clipboardText = new StringBuilder();

            //Copier chaque NIU suivi d'un retour de ligne pour copier dans Excel
            foreach (Intervenant iterv in listeIntervenants)
            {
                clipboardText.Append(iterv.Niu + "\n");
            }

            Clipboard.SetText(clipboardText.ToString());
            addLog("Résultats copiés dans le presse-papier!", greenLogBrush);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (worker != null)
                worker.closeDriver();
        }

        #endregion

        #region Traitement

        private void IntervenantIndividuel_PropertyChange(object sender, PropertyChangedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                ResultatTextBox.Text = ((Intervenant)sender).Niu;
            }));
        }

        private bool validerFormulaireIndividuel()
        {
            bool hasEmptyFeilds = (NomTextBox.Text == "" || PrenomTextBox.Text == "" || 
                                    NaissanceTextBox.Text == "" || IdentifiantTextBox.Text == "");
            return (OrdreProfComboBox.SelectedIndex != -1 && !hasEmptyFeilds);
        }

        private void setCompareBoutonsEnabled(bool isEnabled)
        {
            ComparerIndividuelButton.IsEnabled = isEnabled;
            ComparerListeButton.IsEnabled = isEnabled;
            ViderListeButton.IsEnabled = isEnabled;
            ImporterCsvButton.IsEnabled = isEnabled;
        }
        
        private void addLog(String logLn, Brush color)
        {
            TextRange range = new TextRange(LogBox.Document.ContentEnd, LogBox.Document.ContentEnd);
            range.Text = "> " + logLn + "\r";
            range.ApplyPropertyValue(TextElement.ForegroundProperty, color);
            LogBox.ScrollToEnd();
        }

        #endregion

        #region Fichiers

        private String choisirEmplacementFichier()
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog.Filter = "Feuille CSV|*.csv";
            openFileDialog.FilterIndex = 1;

            openFileDialog.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog.ShowDialog();

            // Process input if the user clicked OK.
            if (userClickedOK == true)
            {
                // Open the selected file to read.
                return openFileDialog.FileName;
            }
            else
            {
                return "";
            }
        }

        private List<Intervenant> importerFeuilleCSV()
        {
            listeIntervenants.Clear();

            try
            {
                var fs = new FileStream(InputCsvTextBox.Text, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var reader = new StreamReader(fs, System.Text.Encoding.Default, true);
                int nbIntervImported = 0;

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    bool isEmptyLine = true;
                    
                    foreach (String value in values)
                    {
                        if (value.Length != 0)
                        {
                            isEmptyLine = false;
                            break;
                        }
                    }

                    if (!isEmptyLine && values.Length == 9)
                    {
                        List<Identifiant> identifiants = new List<Identifiant>();
                        Intervenant newInterv = new Intervenant
                        {
                            Nom =   values[0],
                            Prenom = values[1],
                            Naissance = values[2].Replace("-","/"),
                            Statut = "ACTIVE"
                        };

                        if (values[3].Length > 0)
                            newInterv.Sexe = values[3].ToUpper()[0];

                        identifiants.Add(new Identifiant(Identifiant.ordreFromName(values[8].Trim()), values[7].Trim()));
                        newInterv.Identifiants = identifiants;
                        listeIntervenants.Add(newInterv);
                        nbIntervImported++;
                    }
                }

                fs.Close();
                addLog(nbIntervImported + " intervenants importés.", greenLogBrush);
            }
            catch (Exception e)
            {
                //Dispatch error message
                addLog("An exception occured loading file:\r" + e.StackTrace, Brushes.Red);
            }

            return null;
        }

        #endregion

        #region Worker events

        private void worker_MessageDispatched(object sender, EventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                MessageEventArgs args = (MessageEventArgs)e;
                addLog(args.message, args.color);
            }));
        }

        private void worker_ExecutionCompleted(object sender, EventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                setCompareBoutonsEnabled(true);
            }));
        }

        #endregion
        
    }
     
    /// <summary>
    /// Classe utilitaire à la manipulation de pages web et action spécifiques.
    /// L'utilisation de ses méthodes est faite dans un thread mais n'éxécute toujours qu'une recherche à la fois.
    /// </summary>
    public class WorkerVerification
    {
        public static String URL_RECHERCHE_ET_RESULTATS = "https://wwwpirsr6.ramq.gouv.qc.ca/prsclient/client/pages/provider_search_list.jsp";
        public static String URL_PROFIL_INTERVENANT = "https://wwwpirsr6.ramq.gouv.qc.ca/prsclient/client/pages/prov_details_frameset.jsp";

        public event EventHandler ExecutionCompleted;
        public event EventHandler MessageDispatched;

        private InternetExplorerDriver driver;
        private SolidColorBrush greenLogBrush;
        private bool verificationRunning;

        #region Gestion driver

        public WorkerVerification()
        {
            greenLogBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#00ff00"));
            verificationRunning = false;
        }

        public void StartDriver()
        {
            // Initialize the Chrome Driver
            var options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            driver = new InternetExplorerDriver(options);
            
            //Go to search page            
            MessageEventArgs args = new MessageEventArgs("Veuillez vous connecter au registre...", greenLogBrush);
            OnMessageDispatched(args);
            driver.Navigate().GoToUrl(URL_RECHERCHE_ET_RESULTATS); 
        }

        public void closeDriver()
        {
            if (driver != null)
            {
                driver.Quit();
            }
        }

        #endregion

        #region Traitement de vérifation

        /// <summary>
        /// Recherche chaque intervenant de la liste dans le registre et met le résultat 
        /// de la comparaison dans sa propriété Niu.
        /// </summary>
        /// <param name="intervenants"></param>
        public void verifierIntervenants(List<Intervenant> intervenants)
        {
            if (!verificationRunning)
            {
                verificationRunning = true;
                int nbErrors = 0;

                for (var i = 0; i < intervenants.Count; i++)
                {
                    Intervenant interv = intervenants[i];
                    OnMessageDispatched(new MessageEventArgs("(" + (i + 1) + "/" + intervenants.Count + ") Searching " +
                                                                        interv.Prenom + " " + interv.Nom, greenLogBrush));

                    try
                    {
                        //Rechercher l'intervanant
                        Intervenant intervenantTrouve = rechercherIntervenant(interv);
                        if (intervenantTrouve != null)
                            interv.Niu = interv.obtenirResultatComparaison(intervenantTrouve);
                        else
                            interv.Niu = "Non trouvé";
                    }
                    catch (Exception e)
                    {
                        //Dispatch error message
                        interv.Niu = "Erreur";
                        OnMessageDispatched(new MessageEventArgs("Exception lors de la vérification:\r" + e.ToString(), Brushes.Red));
                        nbErrors++;
                    }
                }

                if (nbErrors == 0)
                    OnMessageDispatched(new MessageEventArgs("Verification ended with success!", greenLogBrush));
                else
                    OnMessageDispatched(new MessageEventArgs("Verification ended with " + nbErrors + " errors.", Brushes.Yellow));

                verificationRunning = false;
                OnExecutionCompleted(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Permet de cherche une internenant dans le registre. Début par une recherche avec identifiant, sinon tente une 
        /// recherche par nom/prenom/ddn.
        /// </summary>
        /// <param name="intervenant">Intervenant à chercher</param>
        /// <param name="useNameBdOnly">Boolean, utiliser le Nom/Prenom et la date de naissance seulement</param>
        /// <returns></returns>
        private Intervenant rechercherIntervenant(Intervenant intervenant)
        {
            Intervenant resultat = rechercherParIdentifiant(intervenant);

            if (resultat == null)
            {
                //Aucun résultat pour la recherche avec identifiant. Cherche avec nom/prenom/ddn
                resultat = rechercherParNomPrenomDdn(intervenant);
            }
                       
            return resultat;
        }
        
        /// <summary>
        /// Lance une recherche par identifiant.
        /// </summary>
        /// <param name="intervenant">Intervenant à trouver</param>
        /// <returns>Intervenant résultat de la recher ou null si échec</returns>
        private Intervenant rechercherParIdentifiant(Intervenant intervenant)
        {
            Intervenant retour = null;

            //Rechercher avec l'identifiant
            driver.Navigate().GoToUrl(URL_RECHERCHE_ET_RESULTATS);
            var identifiantField = driver.FindElementById("txtPROVIDER_CHID");
            identifiantField.SendKeys(intervenant.Numero);

            //Lancer la recherche
            driver.ExecuteScript("formSubmit();");
            Thread.Sleep(5000);

            if (driver.Url.Contains(URL_PROFIL_INTERVENANT))
            {
                //Intervenant trouvé (peut-être). Récolter les informations de la page de profil
                retour = construireIntervenantFromProfil();
            }
            else if (driver.Url.Contains(URL_RECHERCHE_ET_RESULTATS))
            {
                if (driver.Title.Contains("Résultat de la recherche"))
                {
                    //Trouver le bon intervenant dans la liste des résultats par NOM et PRÉNOM
                    retour = trouverDansListeResultats(intervenant, false);
                }
            }

            return retour;
        }

        /// <summary>
        /// Lancer une recherche par Nom, Prénom, Date de naissance.
        /// </summary>
        /// <param name="intervenant">Intervenant à trouver</param>
        /// <returns>Intervenant résultat de la recher ou null si échec</returns>
        private Intervenant rechercherParNomPrenomDdn(Intervenant intervenant)
        {
            Intervenant retour = null;

            //Entrer les champs de la recherche
            driver.Navigate().GoToUrl(URL_RECHERCHE_ET_RESULTATS);

            //Afficher les champs détaillés
            driver.ExecuteScript("show_div('SearchDetails');");
            Thread.Sleep(200);

            var nomField = driver.FindElementById("txtORGNZTN_NAME_TXT");
            var prenomField = driver.FindElementById("txtPRSN_FIRST_GIVEN_NAME_TXT");
            var ddnMinField = driver.FindElementById("txtEARLIEST_DATE_OF_BIRTH");
            var ddnMaxField = driver.FindElementById("txtLATEST_DATE_OF_BIRTH");
            nomField.SendKeys(intervenant.Nom);
            prenomField.SendKeys(intervenant.Prenom);
            ddnMinField.SendKeys(intervenant.Naissance);
            ddnMaxField.SendKeys(intervenant.Naissance);

            //Lancer la recherche
            driver.ExecuteScript("formSubmit();");
            Thread.Sleep(5000);

            if (driver.Url.Contains(URL_PROFIL_INTERVENANT))
            {
                //Intervenant trouvé (peut-être). Récolter les informations de la page de profil
                retour = construireIntervenantFromProfil();
            }
            else if (driver.Url.Contains(URL_RECHERCHE_ET_RESULTATS))
            {
                if (driver.Title.Contains("Résultat de la recherche"))
                {
                    //Trouver le bon intervenant dans la liste des résultats par IDENTIFIANT
                    retour = trouverDansListeResultats(intervenant, true);
                }
            }

            return retour;
        }

        /// <summary>
        /// Choisi l'intervenant correspondant dans la page de résultat et obtient les
        /// information de son profil si il se trouve dans la liste.
        /// </summary>
        /// <param name="intervenant">Intervenant à trouver</param>
        /// <param name="chercherIdentifiant">FAUX pour recherche par nom, VRAI pour cherche par identifiant</param>
        /// <returns>Intervenant trouvé dans la liste ou null</returns>
        private Intervenant trouverDansListeResultats(Intervenant intervenant, bool chercherParIdentifiant)
        {
            Intervenant intervRetour = null;

            //Éléments d'information pour chaque intervenant 
            ReadOnlyCollection<IWebElement> resultatsNoms = driver.FindElements(By.XPath("//tbody//tbody//strong/table/tbody/tr/td/a"));
            ReadOnlyCollection<IWebElement> resultatsOrdres = driver.FindElements(By.XPath("//tbody//tbody//td[5]/table/tbody/tr[1]/td[2]"));
            ReadOnlyCollection<IWebElement> resultatsNoIdent = driver.FindElements(By.XPath("//tbody//tbody//td[5]/table/tbody/tr[1]/td[1]"));

            //Parcourir les résultats dans la liste
            for (int i = 0; i < resultatsNoms.Count; i++)
            {
                IWebElement elementNom = resultatsNoms[i];
                IWebElement elementOrdre = resultatsOrdres[i];
                IWebElement elementNoIdent = resultatsNoIdent[i];
                string[] nomPrenom = elementNom.Text.Split(',');
                string elementURL = elementNom.GetAttribute("href");    

                if (chercherParIdentifiant)
                {
                    //Recherche par identifiant/ordre
                    if (Utils.equalsInvariant(elementOrdre.Text.Trim(), intervenant.Ordre) &&
                        Utils.equalsInvariant(intervenant.Numero, elementNoIdent.Text))
                    {
                        //L'ordre et le no correspondent, naviger dans l'intervenant, contruire l'intervenant
                        driver.Navigate().GoToUrl(elementURL);
                        intervRetour = construireIntervenantFromProfil();
                        break;
                    }
                }
                else
                {
                    //Recherche par nom/prénom
                    //TODO: Aussi rechercher par ordre professionnel car il est supposé être la juste une fois. (au cas ou erreur dans prénom/nom)
                    if ((Utils.containsInvariant(nomPrenom[0].Trim(), intervenant.Nom) &&
                         Utils.containsInvariant(nomPrenom[1].Trim(), intervenant.Prenom)) || 
                          Utils.equalsInvariant(elementOrdre.Text.Trim(), intervenant.Ordre))
                    {
                        //Le nom correspond, naviger dans l'intervenant, contruire l'intervenant
                        driver.Navigate().GoToUrl(elementURL);
                        intervRetour = construireIntervenantFromProfil();
                        break;
                    }
                }
            }

            return intervRetour;
        }

        /// <summary>
        /// Permet d'obtenir les informations sur l'intervenant sur la page de profil.
        /// </summary>
        /// <returns>Intervenant de la page de profil (null si mauvaise page)</returns>
        private Intervenant construireIntervenantFromProfil()
        {
            Intervenant retour = null;

            if (driver.Url.Contains(URL_PROFIL_INTERVENANT))
            {
                //Attendre la réponse AJAX (tableau affiché)
                Thread.Sleep(3000);
                driver.SwitchTo().Frame("LeftFrame"); 
                new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until<IWebElement>((d) =>
                {
                    return driver.FindElement(By.XPath("//table/tbody"));
                });

                //Récolter les informations de l'intervenant
                //TODO: À vérifier, peut-être pas les bonnes variables javascript, surtout l'index
                string nom = driver.ExecuteScript("return perNameDetArr[0].prsn_surname_txt;").ToString().Trim();
                string prenom = driver.ExecuteScript("return perNameDetArr[0].prsn_first_given_name_txt;").ToString().Trim();
                string dateNaissance = driver.ExecuteScript("return demogDetArr[0].date_of_birth_date;").ToString().Trim();
                string statut = driver.ExecuteScript("return statusDetArr[0].status_type_code;").ToString().Trim();
                string niu = driver.ExecuteScript("return regIdentifierDetArr[0].provider_chid;").ToString().Trim();
                Char sexe = driver.ExecuteScript("return demogDetArr[0].gender_code_desc;").ToString()[0];
                long nbIdentifiants = (long)driver.ExecuteScript("return identifierDetArr.length;");
                List<Identifiant> identifiants = new List<Identifiant>();

                for (int i = 0; i < nbIdentifiants; i++)
                {
                    string ordre = driver.ExecuteScript("return identifierDetArr[" + i + "].identifier_type_code;").ToString();
                    string identifiant = driver.ExecuteScript("return identifierDetArr[" + i + "].provider_chid;").ToString();
                    identifiants.Add(new Identifiant(ordre, identifiant));
                }

                retour = new Intervenant { 
                    Nom = nom, 
                    Prenom = prenom, 
                    Naissance = dateNaissance, 
                    Sexe = sexe, 
                    Identifiants = identifiants,
                    Statut = statut,
                    Niu = niu
                };
            }

            return retour;
        }

        #endregion

        #region Événements

        protected virtual void OnExecutionCompleted(EventArgs e)
        {
            EventHandler handler = ExecutionCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnMessageDispatched(MessageEventArgs e)
        {
            EventHandler handler = MessageDispatched;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion
        
    }

    public class MessageEventArgs : EventArgs
    {
        public String message;
        public Brush color;

        public MessageEventArgs(String pMessage, Brush pColor)
        {
            message = pMessage;
            color = pColor;
        }
    }

    public class ComboBoxChoix
    {
        public string _Key { get; set; }
        public Identifiant.Ordre _Value { get; set; }

        public ComboBoxChoix(string _key, Identifiant.Ordre _value)
        {
            _Key = _key;
            _Value = _value;
        }
    }
}
