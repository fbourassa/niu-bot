﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robot_NIU
{
    public class Intervenant : INotifyPropertyChanged 
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public List<Identifiant> Identifiants { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public Char Sexe { get; set; }
        public string Naissance { get; set; }
        public string Statut { get; set; }
        private string _niu;

        public string Niu { 
            get { return _niu; } 
            set { SetField(ref _niu, value, "Niu"); } 
        }

        /**
        *   Donne l'ordre professionnel du premier identifiant
        */
        public string Ordre { 
            get { 
                String ret = "";

                if (Identifiants != null && Identifiants.Count > 0)
                    ret = Identifiants[0].ordre;

                return ret; 
            } 
        }

        
        /**
        *   Donne le numéro d'indentifiant du premier identifiant
        */
        public string Numero { 
            get { 
                String ret = "";

                if (Identifiants != null && Identifiants.Count > 0)
                    ret = Identifiants[0].numero;

                return ret; 
            } 
        }

        /**
         * Compare deux intevenants, retourne le NIU de l'intervenant comparé si les information correspondent.
         * Sinon retourne les informations en erreur dans une chaine de caractère comme ceci : "Prénom, Nom :  Ginnette; Belisle"
         */
        public String obtenirResultatComparaison(Intervenant comp)
        {
            String chaineRetour;
            List<string> champsEnErreur = new List<string>();
            List<string> chempsEnErrVraiValeur = new List<string>();

            if (Utils.equalsInvariant(comp.Nom, this.Nom) == false)
            {
                champsEnErreur.Add("Nom");
                chempsEnErrVraiValeur.Add(comp.Nom);
            }

            if (Utils.equalsInvariant(comp.Prenom, this.Prenom) == false)
            {
                champsEnErreur.Add("Prénom");
                chempsEnErrVraiValeur.Add(comp.Prenom);
            }

            if (this.Sexe.Equals(comp.Sexe) == false)
            {
                champsEnErreur.Add("Sexe");
                chempsEnErrVraiValeur.Add(comp.Sexe.ToString());
            }

            if (this.Naissance.Equals(comp.Naissance) == false)
            {
                champsEnErreur.Add("Naissance");
                chempsEnErrVraiValeur.Add(comp.Naissance);
            }

            if (this.Statut.Equals(comp.Statut) == false)
            {
                champsEnErreur.Add("Statut");
                chempsEnErrVraiValeur.Add(comp.Statut);
            }

            //Identifier les erreurs d'identifiants...
            foreach (Identifiant ident in Identifiants)
            {
                if (comp.hasCorrespondigIdentifiant(ident) == false)
                {
                    champsEnErreur.Add("Identifiant");

                    //Trouver l'identifiant le plus similaire (risque d'être celui en erreur)
                    foreach (Identifiant identComp in comp.Identifiants)
                    {
                        if (identComp.ordre.Contains(ident.ordre) || ident.numero.Contains(identComp.numero))
                        {
                            chempsEnErrVraiValeur.Add(identComp.numero + " (" + identComp.ordre + ")");
                            break;
                        }
                    }
                }
            }

            //Construit le résultat de la comparaison
            if (champsEnErreur.Count > 0)
            {
                chaineRetour = String.Join(",", champsEnErreur.ToArray()) + " : " +
                                String.Join(";", chempsEnErrVraiValeur.ToArray());
            }
            else
            {
                chaineRetour = comp.Niu;
            }

            return chaineRetour;
        }

        public bool hasCorrespondigIdentifiant(Identifiant identifiant)
        {
            bool hasIdentifiant = false;

            foreach (Identifiant ident in Identifiants)
            {
                if (Utils.containsInvariant(ident.ordre, identifiant.ordre) &&
                    Utils.containsInvariant(ident.numero, identifiant.numero))
                {
                    hasIdentifiant = true;
                    break;
                }
            }

            return hasIdentifiant;
        }

        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
