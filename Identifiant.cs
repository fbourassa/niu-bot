﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robot_NIU
{
    public class Identifiant
    {
        public enum Ordre{ NPME, NPPH, NPIN };

        public string ordre;
        public string numero;

        public Identifiant(string _ordre, string _numero)
        {
            ordre = _ordre;
            numero = _numero;
        }

        public Identifiant()
        {
            // TODO: Complete member initialization
        }

        public static string ordreFromName(string ordreName)
        {
            if (Utils.containsInvariant(ordreName, "SOUT"))
            {
                return "Non professionnel";
            }
            if (Utils.containsInvariant(ordreName, "PHAR"))
            {
                return Ordre.NPPH.ToString();
            }
            if (Utils.containsInvariant(ordreName, "MEDECIN"))
            {
                return Ordre.NPME.ToString();
            }
            if (Utils.containsInvariant(ordreName, "INFIR"))
            {
                return Ordre.NPIN.ToString();
            }
            else
            {
                return "Non professionnel";
            }
        }
    }
}
