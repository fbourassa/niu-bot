﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Robot_NIU
{
    public class Utils
    {
        /// <summary>
        /// Permet de trouver un élément dans une chaine sans tenir compte de la case et des accents.
        /// </summary>
        /// <param name="s1">Chaine à parcourir</param>
        /// <param name="s2">Chaine à trouver</param>
        /// <returns></returns>
        public static  bool containsInvariant(string s1, string s2)
        {
            return (RemoveDiacritics(s1).IndexOf(RemoveDiacritics(s2), StringComparison.CurrentCultureIgnoreCase) >= 0);
        }

        public static  bool equalsInvariant(string s1, string s2)
        {
            return RemoveDiacritics(s1).ToUpper().Equals(RemoveDiacritics(s2).ToUpper());
        }

        private static string RemoveDiacritics(string text)
        {
            return string.Concat(
                text.Normalize(NormalizationForm.FormD)
                .Where(ch => CharUnicodeInfo.GetUnicodeCategory(ch) !=
                                              UnicodeCategory.NonSpacingMark)
              ).Normalize(NormalizationForm.FormC);
        }
    }
}
